package in.iblitz.sample.roomlib.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.iblitz.sample.roomlib.database.entitiy.Movie;


@Dao
public interface MovieDao {

    @Query("SELECT * FROM movie")
    List<Movie> getAllMovies();

    @Query("SELECT * FROM movie WHERE movie_name LIKE :name LIMIT 1")
    Movie findByName(String name);

    @Insert
    void insertAll(Movie... movie);

    @Insert
    void insert(Movie movie);

    @Delete
    void delete(Movie user);
}
