package in.iblitz.sample.roomlib.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import in.iblitz.sample.roomlib.database.dao.MovieDao;
import in.iblitz.sample.roomlib.database.entitiy.Movie;

@Database(entities = {Movie.class}, version = 1)
public abstract class DBMovies extends RoomDatabase {
    public abstract MovieDao movieDao();
}
